# DevNet Express STL - Oct 2018

Welcome to the 2-day DevNet Express hosted by Cisco. We will be using the following documentation for the Introduction to Ansible for ACI training module, as well as the labs at [Learning Labs](https://learninglabs.cisco.com/modules/ansible-aci-intro). 

### [Lab Setup](0-Setup.md)

1. Within your ubuntu host, start a terminal for coding, and choose python 2. 
1. Change directory to /home/cisco/CiscoDevNet/code/ `cd /home/cisco/CiscoDevNet/code/`
1. Download code from https://gitlab.com/securenetwrk/201810-stl.git `git clone https://gitlab.com/securenetwrk/201810-stl.git`
1. Change into new directory `cd 201810-stl/intro-ansible`

You are now ready for the learning labs. 

<!-- ### [YAML Basics](1-yaml.md)

### [Lab 1 - NXOS](https://learninglabs.cisco.com/tracks/devnet-express-dci/dne-dci-intro-nx-os/dne-dci-intro-nxos-06_ansible-nxapi/step/2)

### [Lab 2 - NXOS](https://learninglabs.cisco.com/tracks/devnet-express-dci/dne-dci-intro-nx-os/dne-dci-intro-nxos-06_ansible-nxapi/step/4)
 -->

<!-- ---

## Ansible for IOS XE Module: 

From Ubuntu: 
1. git clone https://github.com/CiscoDevNet/dnav3-code
1. cd dnav3-code
1. git checkout intro-ansible

### [Draft Module](https://learninglabs.cisco.com:8867/tracks/dnav3-draft-track)

--or-- 

### [Lab 1 - IOS-XE](intro-ansible-iosxe-labs/ansible-ios-modules/1-intro.md)

### [Lab 2 - IOS-XE](intro-ansible-iosxe-labs/ansible-netconf/1-intro.md)

### [Mission 1 - IOS-XE](intro-ansible-iosxe-labs/ansible-mission/1-intro.md)
-->

---
